module.exports = (function () {
    "use strict";

    function report(filename, line, col, severity, message) {
        console.log(`${filename}:${line}:${col}: ${severity === 0 ? "warning" : "error"}: ${message}`);
    }

    return Object.freeze({
        report
    })
}());