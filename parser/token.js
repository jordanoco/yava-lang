module.exports = (function () {
    "use strict";
    function Token(type, value, line, col) {
        if (!(this instanceof Token)) {
            return new Token(type, value, line, col);
        }
        this.type = type;
        this.value = value;
        this.line = line;
        this.col = col;
    }

    return Token;
}());