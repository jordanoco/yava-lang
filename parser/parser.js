const TokenTypes = require("./token-types.js");
const ErrorHandler = require("./errorhandler.js");
module.exports = (function () {
    "use static";
    function Parser(lexer) {
        if (!(this instanceof Parser)) return new Parser(lexer);
        this.tokens = lexer.run();
        this.filename = lexer.filename;
        this.pos = 0;
    }

    Parser.prototype.prev = function () {
        return this.tokens[this.pos - 1];
    };

    Parser.prototype.end = function () {
        return this.peek().type === TokenTypes.EOS;
    };

    Parser.prototype.advance = function () {
        if (!this.end()) this.pos += 1;
        return this.prev();
    };

    Parser.prototype.peek = function () {
        return this.tokens[this.pos];
    };

    Parser.prototype.check = function (type) {
        if (this.end()) return false;
        return this.peek().type === type;
    };

    Parser.prototype.match = function () {
        for (let i = 0; i < arguments.length; i += 1) {
            if (this.check(arguments[i])) {
                this.advance();
                return true;
            }
        }
        return false;
    };
    
    Parser.prototype.run = function () {
        let toplevel = {type: "program", scope: [], body: []};
        
        return toplevel;
    };

    return Parser;
}());
