module.exports = (function () {
    "use strict";
    function Scanner(filename, input) {
        if (!(this instanceof Scanner)) return new Scanner(filename, input);
        this.filename = filename;
        this.source = input;
        this.pos = 0;
        this.line = 1;
        this.col = 0;
    }

    Scanner.prototype.next = function () {
        var ch = this.source[this.pos];
        this.pos += 1;
        if (ch == "\n") {
            this.line += 1;
            this.col = 0;
        } else {
            this.col += 1;
        }        
        return ch;
    };

    Scanner.prototype.peek = function () {
        return this.source[this.pos];
    };

    Scanner.prototype.eof = function () {
        return this.pos > this.source.length - 1;
    };

    return Scanner;
}());