const Token = require("./token.js");
const TokenTypes = require("./token-types.js");
const ErrorHandler = require("./errorhandler.js");
module.exports = (function () {
    "use strict";
    function Lexer(scanner) {
        if (!(this instanceof Lexer)) return new Lexer(scanner);
        this.scanner = scanner;
        this.start = 0;
        this.end = 0;
        this.tokens = [];
    }

    Lexer.prototype.addToken = function (type, value, line, col) {
        this.tokens.push(new Token(type, value, line, col));
    };

    Lexer.prototype.peek = function () {
        return this.scanner.peek();
    };

    Lexer.prototype.advance = function () {
        this.end += 1;
        return this.scanner.next();
    };

    Lexer.prototype.eos = function () {
        return this.scanner.eof();
    };

    Lexer.prototype.match = function (expected) {
        if (this.eos()) return false;
        if (this.peek() !== expected) return false;
        return true;
    };

    function isAlpha(ch) {
        return /[a-zA-Z]/.test(ch);
    }

    function isNumeric(ch){
        return /[0-9]/.test(ch);
    }

    function isFloatNumeric(ch) {
        return /[0-9.]/.test(ch);
    }

    function isAlphaNumeric(ch) {
        return (isAlpha(ch) || isNumeric(ch));
    }

    Lexer.prototype.run = function () {
        let lexerror = false;
        while (!this.scanner.eof() && !lexerror) {
            this.start = this.end;
            let ch = this.advance();
            let col = this.scanner.col;
            let line = this.scanner.line;
            if (isAlpha(ch)) {
                while (!this.eos() && isAlpha(this.peek())) this.advance();
                let raw = this.scanner.source.substring(this.start, this.end);
                switch (raw) {
                    case "class": this.addToken(TokenTypes.CLASS, raw, line, col); break;
                    case "public": this.addToken(TokenTypes.PUBLIC, raw, line, col); break;
                    case "new": this.addToken(TokenTypes.NEW, raw, line, col); break;
                    case "if": this.addToken(TokenTypes.IF, raw, line, col); break;
                    case "else": this.addToken(TokenTypes.ELSE, raw, line, col); break;
                    case "for": this.addToken(TokenTypes.FOR, raw, line, col); break;
                    case "continue": this.addToken(TokenTypes.CONTINUE, raw, line, col); break;
                    case "break": this.addToken(TokenTypes.BREAK, raw, line, col); break;
                    case "true": this.addToken(TokenTypes.TRUE, raw, line, col); break;
                    case "false": this.addToken(TokenTypes.FALSE, raw, line, col); break;
                    case "try": this.addToken(TokenTypes.TRY, raw, line, col); break;
                    case "throw": this.addToken(TokenTypes.THROW, raw, line, col); break;
                    case "static": this.addToken(TokenTypes.STATIC, raw, line, col); break;
                    case "extends": this.addToken(TokenTypes.EXTENDS, raw, line, col); break;
                    case "super": this.addToken(TokenTypes.SUPER, raw, line, col); break;
                    case "num": this.addToken(TokenTypes.TNUM, raw, line, col); break;
                    case "string": this.addToken(TokenTypes.TSTRING, raw, line, col); break;
                    case "bool": this.addToken(TokenTypes.TBOOL, raw, line, col); break;
                    case "void": this.addToken(TokenTypes.TVOID, raw, line, col); break;
                    case "null": this.addToken(TokenTypes.NULL, raw, line, col); break;
                    case "immutable": this.addToken(TokenTypes.IMMUTABLE, raw, line, col); break;
                    case "struct": this.addToken(TokenTypes.STRUCT, raw, line, col); break;
                    default: this.addToken(TokenTypes.LITERAL, raw, line, col);
                }
            } else if (isNumeric(ch)) {
                let dotreached = false;
                while (!this.eos()) {
                    if (!isFloatNumeric(this.peek())) break;
                    if (this.peek() === "." && dotreached == true) break;
                    if (this.peek() == "." && dotreached == false) dotreached = true;
                    this.advance();
                }
                let raw = this.scanner.source.substring(this.start, this.end);
                let value = Number(raw);
                this.addToken(TokenTypes.NUMBER, value, line, col);
            } else if (ch === "\"") {
                while (!this.eos() && this.peek() != "\"") this.advance();
                let raw = this.scanner.source.substring(this.start + 1, this.end);
                this.addToken(TokenTypes.STRING, raw, line, col);
                if (this.eos() && this.peek() != "\"") {
                    ErrorHandler.report(this.scanner.filename, line, col, 1, "missing \" termination character");
                    lexerror = true;
                    this.tokens.length = 0;
                }
                this.advance();
            } else if (ch === "'") {
                while (!this.eos() && this.peek() != "'") this.advance();
                let raw = this.scanner.source.substring(this.start + 1, this.end);
                this.addToken(TokenTypes.STRING, raw, line, col);
                this.advance();
                if (this.eos() && this.peek() != "'") {
                    ErrorHandler.report(this.scanner.filename, line, col, 1, "missing ' termination character");
                    lexerror = true;
                    this.tokens.length = 0;
                }
            } else if (ch === "/") {
                if (this.match("/")) {
                    while (!this.eos() && this.peek() != "\n") this.advance();
                } else {
                    this.addToken(TokenTypes.SLASH, ch, line, col);
                }
            } else if (ch === "(") {
                this.addToken(TokenTypes.LPAREN, ch, line, col);
            } else if (ch === ")") {
                this.addToken(TokenTypes.RPAREN, ch, line, col);
            } else if (ch === "{") {
                this.addToken(TokenTypes.LBRACE, ch, line, col);
            } else if (ch === "}") {
                this.addToken(TokenTypes.RBRACE, ch, line, col);
            } else if (ch === "[") {
                if (this.match("]")) {
                    this.advance();
                    this.addToken(TokenTypes.DSQUARE, "[]", line, col);
                } else {
                    this.addToken(TokenTypes.LSQUARE, "[", line, col);
                }
            } else if (ch === "]") {
                this.addToken(TokenTypes.RSQUARE, ch, line, col);
            } else if (ch === ","){
                this.addToken(TokenTypes.COMMA, ch, line, col);
            } else if (ch === ";") {
                this.addToken(TokenTypes.SEMICOLON, ch, line, col);
            } else if (ch === "*") {
                this.addToken(TokenTypes.STAR, ch, line, col);
            } else if (ch === ".") {
                if (this.match(".")) {
                    this.advance();
                    this.addToken(TokenTypes.DDOT, "..", line, col);
                } else {
                    this.addToken(TokenTypes.DOT, ".", line, col);
                }
            } else if (ch === "+") {
                if (this.match("=")) {
                    this.advance();
                    this.addToken(TokenTypes.PEQUAL, "+=", line, col);
                } else if (this.match("+")) {
                    this.advance();
                    this.addToken(TokenTypes.DPLUS, "++", line, col);
                } else {
                    this.addToken(TokenTypes.PLUS, "+", line, col);
                }
            } else if (ch === "-") {
                if (this.match("=")) {
                    this.advance();
                    this.addToken(TokenTypes.MEQUAL, "-=", line, col);
                } else if (this.match("-")) {
                    this.advance();
                    this.addToken(TokenTypes.DMINUS, "--", line, col);
                } else {
                    this.addToken(TokenTypes.MINUS, "-", line, col);
                }
            } else if (ch === "|") {
                if (this.match("|")) {
                    this.advance();
                    this.addToken(TokenTypes.DPIPE, "||", line, col);
                } else {
                    this.addToken(TokenTypes.BXOR, "|", line, col);
                }
            } else if (ch === "&") {
                if (this.match("&")) {
                    this.advance();
                    this.addToken(TokenTypes.DAMP, "&&", line, col);
                } else {
                    this.addToken(TokenTypes.BAND, "&", line, col);
                }
            } else if (ch === ":") {
                if (this.match(":")) {
                    this.advance();
                    this.addToken(TokenTypes.DCOLON, "::", line, col);
                } else {
                    ErrorHandler.report(this.scanner.filename, line, col, 1, "saw ':', expected '::'");
                    lexerror = true;
                    this.tokens.length = 0;
                }
            } else if (ch === "<") {
                if (this.match("=")) {
                    this.advance();
                    this.addToken(TokenTypes.LEQUAL, "<=", line, col);
                } else if (this.match("<")) {
                    this.advance();
                    this.addToken(TokenTypes.BLSHIFT, "<<", line, col);
                } else {
                    this.addToken(TokenTypes.LESS, "<", line, col);
                }
            } else if (ch === ">") {
                if (this.match("=")) {
                    this.advance();
                    this.addToken(TokenTypes.GEQUAL, ">=", line, col);
                } else if (this.match(">")) { 
                    this.advance();
                    if (this.match(">")) {
                        this.advance();
                        this.addToken(TokenTypes.BZSHIFT, ">>>", line, col);
                    } else {
                        this.advance();
                        this.addToken(TokenTypes.BRSHIFT, ">>", line, col);
                    }
                } else {
                    this.addToken(TokenTypes.GREATER, ">", line, col);
                }
            } else if (ch === "=") {
                if (this.match("=")) {
                    this.advance();
                    this.addToken(TokenTypes.EEQUAL, "==", line, col);
                } else {
                    this.addToken(TokenTypes.EQUAL, "=", line, col);
                }
            } else if (ch === "!") {
                if (this.match("=")) {
                    this.advance();
                    this.addToken(TokenTypes.BEQUAL, "!=", line, col);
                } else {
                    this.addToken(TokenTypes.BANG, "!", line, col);
                }
            }
        }

        this.addToken(TokenTypes.EOS, undefined, undefined, undefined);
        return this.tokens;
    };

    return Lexer;
}());