yava-lang
=========
Jordan Ocokoljic <self@jordano.co>
Version 1, 7/3/18

Yava is a programming language that compiles to JavaScript. The guiding
principle behind the language is that it should be as expressive and powerful 
as possible without sacrificing safety and security. It is loosely inspired by
languages such as C# and C++.

The implementation is designed to be as simple as possible, and as modular as
possible. This allows it to be studied by people interested in the design and
implementation of a transpiler.

Yava is type safe. This is to keep you safe, and sane. Most of the problems 
associated with JavaScript are related to its weak typing. yava is strongly
typed, with minimal flexiblity in this regard. It has an entry point, simiar
to C or C++. This is to enforce a scope at even the highest level, to prevent
the use of globals.

Unlike TypeScript, Yava is not compatible with existing JavaScript libraries,
this is because they are not held to the same standard that Yava is, and thus
they cannot be trusted to be safe. Also because this is a first attempt, and
writing the compatability is difficult, this may change in the future.