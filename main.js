const fs = require("fs");
const path = require("path");
const Scanner = require("./parser/scanner.js");
const Lexer = require("./parser/lexer.js");
const Parser = require("./parser/parser.js");

var source = fs.readFileSync(process.argv[2], "utf8");
var scanner = new Scanner(path.basename(process.argv[2]), source);
var lexer = new Lexer(scanner);
var parser = new Parser(lexer);
var ast = parser.run();

console.log(ast);